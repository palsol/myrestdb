/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;



/**
 *
 * @author palsol
 */
public class Query {
    
     public static final String DISH_FIND_ALL = "SELECT d FROM Dish d";
     public static final String DISH_FIND_BY_PART_NAME = "SELECT d FROM Dish d WHERE d.name LIKE :name";
     public static final String DISH_FIND_BY_ID = "SELECT d FROM Dish d WHERE d.id = :id";
     public static final String DISH_FIND_BY_NAME =  "SELECT d FROM Dish d WHERE d.name = :name";
     public static final String DISH_FIND_BY_PRICE = "SELECT d FROM Dish d WHERE d.price = :price";
     
     public static final String  CATEGORY_FIND_ALL = "SELECT c FROM Category c";
     public static final String  CATEGORY_FIND_BY_ID = "SELECT c FROM Category c WHERE c.id = :id";
     public static final String  CATEGORY_FIND_BY_NAME = "SELECT c FROM Category c WHERE c.name = :name";
}
