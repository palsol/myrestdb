/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

/**
 *
 * @author palsol
 */
final class  ErrorMsg {
    
    public static final String VALID = "valid";
    public static final String NO_PRICE = "Please input price.";
    public static final String WRONG_PRICE = "Wrong price.";
    public static final String NO_NAME = "Please input name.";
    public static final String LESS_THREE_SYMB = "Name must be longer than 3 symbols.";
 
}
