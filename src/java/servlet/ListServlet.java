/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.CategoryBean;
import static bean.CategoryBean.LOG;
import bean.DishBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author palsol
 */
@WebServlet(name = "ListServlet", loadOnStartup = 1, urlPatterns = {"/ListServlet"})
public class ListServlet extends HttpServlet {

    @EJB
    DishBean dishBean;
    @EJB
    CategoryBean categoryBean;

    String searchPart = "";
    public static final Logger LOG=Logger.getLogger(ListServlet.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String error = "";

        String[] selectedItems = request.getParameterValues("selectedItems");

        if ("Save".equalsIgnoreCase(request.getParameter("action"))
                || "Add".equalsIgnoreCase(request.getParameter("action"))
                || "Remove".equalsIgnoreCase(request.getParameter("action"))) {

            LOG.info(request.getParameter("error"));
            try {
                error = request.getParameter("error");
            } catch (NullPointerException e) {
            }

             LOG.info(request.getParameter("action"));

            response.sendRedirect("ListServlet?searchPart=" + searchPart + "&error=" + error + "&action=Search");
        } else {

            if (selectedItems != null) {
                for (String selectedItem : selectedItems) {
                     LOG.info(selectedItem);
                }
            }

            String action = request.getParameter("action");
            if ("Delete".equalsIgnoreCase(action)) {
                if (selectedItems != null) {
                    for (String selectedItem : selectedItems) {
                        int delId = Integer.parseInt(selectedItem);
                         LOG.info(delId);
                        dishBean.deleteDish(delId);
                    }
                }

                response.sendRedirect("ListServlet?searchPart=" + searchPart + "&error=" + error + "&action=Search");

            } else {

                if ("Search".equalsIgnoreCase(action)) {
                    searchPart = request.getParameter("searchPart");
                }

                if ("Show All".equalsIgnoreCase(action)) {
                    searchPart = "";
                }
                request.setAttribute("dishes", dishBean.getDishes(searchPart));
                request.setAttribute("categories", categoryBean.getAllCategories());
                request.setAttribute("searchPart", searchPart);

                error = request.getParameter("error");
                if (error == null) {
                    error = "";
                }
                request.setAttribute("error", error);
                request.getRequestDispatcher("list.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
