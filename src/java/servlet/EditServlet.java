/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.CategoryBean;
import static bean.CategoryBean.LOG;
import bean.DishBean;
import entity.Dish;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author palsol
 */
@WebServlet(name = "EditServlet", urlPatterns = {"/EditServlet"})
public class EditServlet extends HttpServlet {

    @EJB
    DishBean dishBean;
    @EJB
    CategoryBean categoryBean;

    String errorStr = "valid";
    public static final Logger LOG=Logger.getLogger(EditServlet.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String dishIdStr = request.getParameter("editId");
        errorStr = ErrorMsg.VALID;
        LOG.info(dishIdStr);
        
        int dishId = 0;
        try {
            dishId = Integer.parseInt(dishIdStr);
        } catch (NumberFormatException e) { 
      }

        String action = request.getParameter("action");
        if ("Save".equalsIgnoreCase(action) || "Add".equalsIgnoreCase(action)) {
            double price = 0;
            if (!request.getParameter("price").isEmpty()) {
                String priceStr = request.getParameter("price");

                try {
                    price = Double.parseDouble(priceStr);
                } catch (NumberFormatException e) {
                    price = -1;
                }

                if (price >= 0) {
                    if (!request.getParameter("name").isEmpty()) {
                        String name = request.getParameter("name");
                        if (name.length() > 3) {

                            Dish newDish = new Dish(name, price);
                            int categoryId = 0;
                            String categoryIdStr = request.getParameter("categoryId");
                            if (categoryIdStr != null && !categoryIdStr.equals("")) {
                                categoryId = Integer.parseInt(categoryIdStr);
                                newDish.setCategory(categoryBean.getCategory(categoryId));
                            }

                            if ("Save".equalsIgnoreCase(action)) {
                                newDish.setId(dishId);
                                dishBean.editDish(newDish);
                                request.setAttribute("EDIT", "true");
                            } else {
                                dishId = dishBean.addDish(newDish);
                            }
                        } else {
                            errorStr = ErrorMsg.LESS_THREE_SYMB;
                        }
                    } else {
                        errorStr = ErrorMsg.NO_NAME;
                    }
                } else {
                    errorStr = ErrorMsg.WRONG_PRICE;
                }
            } else {
                errorStr = ErrorMsg.NO_PRICE;
            }
        }

        if ("Remove".equalsIgnoreCase(action)) {
            if (dishBean.getDish(dishId) != null) {
                 LOG.info(dishId);
                dishBean.deleteDish(dishId);
            }
        }

        Dish dish = dishBean.getDish(dishId);
        request.setAttribute("dish", dish);
        request.setAttribute("searchPart", request.getParameter("searchPart"));
        //request.setAttribute("error", errorStr);
        request.getRequestDispatcher("ListServlet?error=" + errorStr).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
