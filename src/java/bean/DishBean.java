/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Dish;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author palsol
 */
@Stateless
public class DishBean {

    // Injected database connection:
    @PersistenceContext
    private EntityManager entityManager;

    public int addDish(Dish dish) {
        entityManager.persist(dish);
        entityManager.flush();      
        return dish.getId();
    }

    public void editDish(Dish dish) {
        entityManager.merge(dish);
        entityManager.flush();
    }

    public void deleteDish(int DishId) {

        entityManager.remove(getDish(DishId));
        entityManager.flush();
    }

    public Dish getDish(int DishId) {
        entityManager.flush();
        return entityManager.find(Dish.class, DishId);
    }

    public List<Dish> getAllDishes() {
        entityManager.flush();
        List<Dish> dishes = entityManager.createNamedQuery("Dish.findByPartName").getResultList();
        return dishes;
    }

    public List<Dish> getDishes(String searchPart) {
        entityManager.flush();
        List<Dish> dishes;
        if (searchPart != null || !searchPart.isEmpty()) {
            dishes = entityManager.createNamedQuery("Dish.findByPartName").setParameter("name", "%" + searchPart + "%").getResultList();
        } else {
            dishes = entityManager.createNamedQuery("Dish.findByPartName").getResultList();
        }

        return dishes;
    }
}
