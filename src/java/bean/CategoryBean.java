/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Category;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Logger;

/**
 *
 * @author palsol
 */
@Stateless
public class CategoryBean {

    @PersistenceContext
    private EntityManager entityManager;
    public static final Logger LOG=Logger.getLogger(CategoryBean.class);

    public Category getCategory(int CategoryId) {
        entityManager.flush();
        return entityManager.find(Category.class, CategoryId);
    }

    public List<Category> getAllCategories() {
        entityManager.flush();
        List<Category> categories = entityManager.createNamedQuery("Category.findAll").getResultList();
        categories.add(0, null);
        LOG.info("Category.findAll");
        return categories;
    }
}
