<%-- 
    Document   : list
    Created on : Apr 17, 2016, 12:30:07 PM
    Author     : palsol
--%>

<%@page import="java.util.*,entity.Dish"%>
<%@taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style type="text/css">
            .block1{ 
                width: 400px; 
                background: #F2F2F2;
                color: #262626;
                padding: 5px;
                padding-right: 20px; 
                border: solid 1px black; 
                float: left;
            }
            .block2{ 
                width: 230px; 
                background: #CB3333; 
                color: #f5f5f5;
                padding: 5px; 
                border: solid 1px black; 
                float: left; 
                position: relative;
                top: 200px; 
                left: -242px; 
            }

            .block3{ 
                width: 230px; 
                background: #CB3333; 
                color: #f5f5f5;
                padding: 5px; 
                border: solid 1px black; 
                float: left; 
                position: relative;
                top: 40px; 
                left: 0px; 
            }

            tr div{               
                width: 100px; /* Ширина элемента */
                table-layout: fixed;
            }

            th {
                background: #afd792; /* Цвет фона */
                color: #333;  /* Цвет текста */
            }
            .block1 tr:hover {
                background: #515151; /* Цвет фона при наведении */
                color: #fff; /* Цвет текста при наведении */
            }
        </style>
        <script>
            function tdclick(td)
            {
                editServ.editId.value = td;
                editServ.submit();
            }
        </script>
    </head>
    <body>   
        <form id='listServ' action="./ListServlet" method="GET">    
            <div class="block1">    
                <p>
                    <input type="text" name="searchPart" value="${searchPart}" />
                    <input type="submit" name="action" value="Search" />
                    <input type="submit" name="action" value="Show all" />
                </p>          
                <br>              
                <table width="100%" cellspacing="0" cellpadding="4" border="1">
                    <th width="20">name</th>
                    <th>price</th>
                    <th>meal time</th>

                    <th colspan="1">
                        <input type="submit" name="action" value="Delete" />                       
                    </th>                

                    <s:forEach items="${dishes}" var="dishEl">              
                        <tr>                                              
                            <td width="100" onclick=tdclick("${dishEl.id}")><div>${dishEl.name}</div></td>
                            <td onclick=tdclick("${dishEl.id}")>${dishEl.price}</td>  
                            <td onclick=tdclick("${dishEl.id}")>${dishEl.category.name}</td>    

                            <td align="center">  
                                <input type="checkbox" name="selectedItems" value="${dishEl.id}"/> 
                            </td>

                        </tr>
                    </s:forEach>                             
                </table>  
            </div>
        </form>

        <div class="block3">
            <strong>EDIT DISH</strong>
            <form id='editServ' action="./EditServlet" method="GET">  
                <input type="hidden" name="editId" value="${dish.id}">         
                <table>     
                    <tr>
                        <td>Name</td>
                        <td><input type="text" name="name" value="${dish.name}" /></td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td><input type="text" name="price" value="${dish.price}" /></td>
                    </tr>

                    <strong>Select a category</strong>
                    <select name="categoryId">
                        <s:forEach items="${categories}" var="categoryEl">
                            <option value="${categoryEl.id}">${categoryEl.name}</option>
                        </s:forEach>                   
                    </select>

                    <tr>
                        <td colspan="2">                           
                            <input type="submit" name="action" value="Save" /> 
                            <input type="submit" name="action" value="Remove" /> 
                            <input type="submit" name="action" value="Add" /> 
                        </td>                
                    </tr>   
                    
                    <h0> <%=request.getAttribute("error")%> </h0>
                    
                </table>                          
            </form> 
        </div>       
    </body>
</html>
